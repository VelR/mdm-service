import React from 'react';
import styled from 'styled-components'
import MaterialIcon from 'material-icons-react';
import Scrollchor from 'react-scrollchor';

import {Column} from "./containers/Containers";

class PhoneHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
    };

    isOpenChange = () => {
        this.setState({isOpen: !this.state.isOpen});
    };

    render() {
        if (this.state.isOpen) {
            return (
                <HeaderWrapper>
                    <ButtonWrapper>
                        <BurgerButton onClick={this.isOpenChange}>
                            <MaterialIcon icon="dehaze" color="white" size='35px'/>
                        </BurgerButton>
                        <MenuWrapper isOpen={this.state.isOpen}>
                            <NavItem to="main">
                                <div onClick={this.isOpenChange}>
                                    Главная
                                </div>
                            </NavItem>
                            <NavItem to="promotion">
                                <div onClick={this.isOpenChange}>
                                    Акции
                                </div>
                            </NavItem>
                            <NavItem to="service">
                                <div onClick={this.isOpenChange}>
                                    Сервис
                                </div>
                            </NavItem>
                            <NavItem to="about">
                                <div onClick={this.isOpenChange}>
                                    О нас
                                </div>
                            </NavItem>
                            <NavItem to="contacts">
                                <div onClick={this.isOpenChange}>
                                    Контакты
                                </div>
                            </NavItem>
                        </MenuWrapper>
                    </ButtonWrapper>
                </HeaderWrapper>
            );
        }
        else {
            return (
                <HeaderWrapper>
                    <ButtonWrapper>
                        <BurgerButton onClick={this.isOpenChange}>
                            <MaterialIcon icon="dehaze" color="white" size='35px'/>
                        </BurgerButton>
                    </ButtonWrapper>
                </HeaderWrapper>
            );
        }

    }
}

//#region Styled

const NavItem = styled(Scrollchor)`
  padding: 1em 1.5em;
  cursor: pointer;
  text-align: center;
  color: ${props => props.theme.lightFontColor};
  transition: color 0.4s ease, border 0.4s ease;
  border: 1px solid transparent;
  text-decoration: none;
  :hover{
      text-decoration: none;
      border-bottom: 1px solid ${props => props.theme.linkHoverColor};
  }
  :active{
      transition: color 0s ease, border 0s ease;
      color: ${props => props.theme.linkHoverColor};
      background-color: ${props => props.theme.linkHoverColor};
      border: 1px solid ${props => props.theme.linkHoverColor};
      }
  }
`;

const MenuWrapper = Column.extend`
  padding: 0.5rem;
  width: 150px;
`;

const ButtonWrapper = Column.extend`
    top: 0;
    left: 0;
    z-index: 999;
    margin: 1em 1em 1em 0;
    padding: 0 0.7em 0 1em;
    border-bottom-right-radius: 1.5em;
    border-top-right-radius: 1.5em;
    background: rgba(0, 0, 0, 0.80);
`;


const BurgerButton = styled.div`
  padding: 0.5rem;
  float: right;
  text-align: center;
  background: transparent;
  border-width: 0;
  :hover{
  cursor: pointer;
  border-width: 0;  
  }  
`;


const HeaderWrapper = styled.div`
  position: absolute;  
`;


//#endregion

export default PhoneHeader;