import React, {Component} from 'react';
import styled from 'styled-components'
import {Row} from "./Containers";
import MaterialIcon from 'material-icons-react';

class AdvantagesCard extends Component {
    render() {
        return (
            <Wrapper>
                <TextWrapper>
                    <Icon>
                        <MaterialIcon icon={this.props.icon} color="black" size='large'/>
                    </Icon>
                    <FirstText>
                        {this.props.firstText}
                    </FirstText>
                    <SecondText>
                        {this.props.secondText}
                    </SecondText>
                </TextWrapper>
            </Wrapper>
        )
    }
}

//#region Styled

const Wrapper = Row.extend`
  color: black;
  text-align: center;
  border: 1px white solid;
  background: rgba(255,255,255,0.5);
  padding: 2em 0;
`;

const Icon = styled.div`
  width: 100%;
`;

const TextWrapper = styled.div`
  width: 100%;
  text-align: center;
`;

const FirstText = styled.div`
  text-align: center;
`;

const SecondText = styled.div`
  font-size: 18px;
  font-weight: bold;
`;

//#endregion

export default AdvantagesCard;
