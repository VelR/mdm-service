import React, {Component} from 'react';
import styled from 'styled-components'
import {Column} from "../containers/Containers";

class Card extends Component {
    render() {
        return (
            <Background image={this.props.image}>
                <Obscurer>
                    <Text>
                        {this.props.text}
                    </Text>
                </Obscurer>
            </Background>
        )
    }
}

//#region Styled

const Background = styled.div`
  width: 100%;
  background: url(${props => props.image}) 100%;
  background-size: cover;
`;

const Obscurer = Column.extend`
  width: 100%;
  height: 100%;
  cursor: pointer;
  background: rgba(0,0,0,0.5);
  transition: background-color 0.3s ease;
  :hover{
    background: rgba(0,0,0,0.0);
  }
`;

const Text = styled.div`
  color: white;
  margin: auto;
  padding: 1em;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
  text-align: center;
`;

//#endregion

export default Card;
