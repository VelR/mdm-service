import React, {Component} from 'react';
import styled from 'styled-components'
import {Column} from "../containers/Containers";
import MaterialIcon from 'material-icons-react';
import {Row} from "./Containers";

class QuestionCard extends Component {
    render() {
        return (
            <Row>
                <MaterialIcon icon="question_answer" color="white" size='40'/>
                <Wrapper>
                    <Question>
                        {this.props.question}
                    </Question>
                    <Response>
                        - {this.props.response}
                    </Response>
                </Wrapper>
            </Row>
        )
    }
}

//#region Styled

const Wrapper = Column.extend`
  border: 3px white solid;
  padding: 1rem;
  margin: 0 0 1rem 0;
  max-width: 50%;
  border-bottom-left-radius: 3rem;
  border-bottom-right-radius: 30% 10%;
  border-top: 0;
  border-bottom: 1;
  border-left: 0;
  border-right: 0;
`;

const Question = styled.div`
  margin-top: auto;
  font-weight: bold;
  color: white;
`;

const Response = styled.div`
  color: #ccc;
  margin: 0.5rem 0.5rem 0 2rem;
`;

//#endregion

export default QuestionCard;
