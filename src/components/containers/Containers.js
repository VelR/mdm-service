import styled from "styled-components";

export const Page = styled.div`
display: flex;
width: 80%;
margin: auto;
flex-direction: column;
height: inherit;
:first-child{
flex: 1;
}
@media (min-width: 1200px) {
      width: 90%;
    }
@media (max-width: 1200px) {
      width: 90%;
    } 
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;  
  :first-child{
  flex: 1;
  }
`;

export const Column = styled.div`
  font-size: inherit;
  display: flex;  
  flex-direction: column;
  :first-child{
  flex: 1;
  }
`;


export const DockTop = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    z-index: 100;
    width: 100%;
`;




