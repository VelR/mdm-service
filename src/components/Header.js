import React, {Component} from 'react';
import {DockTop} from "./containers/Containers";
import Media from "react-media";
import PhoneHeader from "./PhoneHeader";
import ScreenHeader from "./ScreenHeader";

class Header extends Component {
    render() {
        return (
            <DockTop>
                <Media query="(min-width: 700px)">
                    {matches => matches ? (
                        <ScreenHeader/>
                    ) : (
                        <PhoneHeader/>
                    )}
                </Media>
            </DockTop>
        )
    }
}

//#region Styled

//#endregion

export default Header;