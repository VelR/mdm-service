import React, {Component} from 'react';
import styled from 'styled-components'
import {Link} from "./Components";
import {Page, Row} from "./containers/Containers";
import Media from "react-media";
import Scrollchor from 'react-scrollchor';
import logo from '../Images/logo.png';

class ScreenHeader extends Component {
    render() {
        return (
            <HeaderWrapper>
                <Page>
                    <Row>
                        <Row>
                            <Logo src={logo} alt="MDM-logo"/>
                            <Media query="(min-width: 900px)">
                                {matches => matches ? (
                                    <CallUs>
                                        Звоните нам!
                                        <Link href="tel:+74842597878">
                                            <AlignText align={"right"}>
                                                +7 (4842) 59-78-78
                                            </AlignText>
                                        </Link>
                                    </CallUs>
                                ) : (<div></div>)}
                            </Media>
                        </Row>
                        <Row>
                            <NavBar>
                                <NavItem to="main">
                                        Главная
                                </NavItem>
                                <NavItem to="promotion" animate={{offset: -70, duration: 600}}>
                                    Акции
                                </NavItem>
                                <NavItem to="service" animate={{offset: -70, duration: 600}}>
                                    Сервис
                                </NavItem>
                                <NavItem to="about" animate={{offset: -69, duration: 600}}>
                                    О нас
                                </NavItem>
                                <NavItem to="contacts" animate={{offset: -70, duration: 600}}>
                                    Контакты
                                </NavItem>
                            </NavBar>
                        </Row>
                    </Row>
                </Page>
            </HeaderWrapper>
        )
    }
}

//#region Styled
const AlignText = styled.div`
  text-align: ${props => props.align};
  margin: 0 0 0 0.5em;
`;

const CallUs = Row.extend`
  white-space: nowrap;
  margin: auto 0;
`;

const HeaderWrapper = styled.div`
  position: absolute;
  background: rgba(0, 0, 0, 0.90);
  color: #fff;
  width: 100%;
  padding: 1vh 0;
`;

const NavBar = Row.extend`
  margin: auto;  
`;

const NavItem = styled(Scrollchor)`
  padding: 0.5em 1em;
  cursor: pointer;
  color: ${props => props.theme.lightFontColor};
  transition: color 0.4s ease, border 0.4s ease;
  border: 1px solid transparent;
  text-decoration: none;
  :hover{
      text-decoration: none;
      border-bottom: 1px solid ${props => props.theme.linkHoverColor};
  }
  :active{
      transition: color 0s ease, border 0s ease;
      color: ${props => props.theme.linkHoverColor};
      background-color: ${props => props.theme.linkHoverColor};
      border: 1px solid ${props => props.theme.linkHoverColor};
      }
  }
`;

const Logo = styled.img`
  margin: auto 0;
  padding: 0 1vh;
  height: 40px;
`;
//#endregion

export default ScreenHeader;