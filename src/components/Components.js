import styled from "styled-components";

export const Link = styled.a`
  text-decoration: none;
  color: ${props => props.theme.linkColor};
  :hover{
  color: ${props => props.theme.linkHoverColor};
  text-decoration: underline;
  }
`;

export const Button = styled.button`
  width: ${props => props.width};
  cursor: pointer;
  padding: 1em 2em;
  margin: 0.5em;
  font-size: ${props => props.fontSize ? props.fontSize : "inherit"};
  border: 1px ${props => props.primary ? props.theme.primaryHoverColor : props.theme.linkColor};  
  border-radius: 3em;
  background: ${props => props.primary ? props.theme.primaryColor : props.theme.lightFontColor};
  color: ${props => props.primary ? props.theme.lightFontColor : props.theme.linkColor};
  :hover{
    color: ${props => props.primary ? props.theme.lightFontColor : props.theme.lightFontColor};
    background: ${props => props.primary ? props.theme.primaryHoverColor : props.theme.linkColor};
    box-shadow: 0.3em 0.3em 0.5em rgba(0,0,0,0.5); /* Параметры тени */
  } 
  :focus{
    outline: none;
  }
`;

export const H1 = styled.div`
  padding: 0 0 0.5em 0;
  font-size: ${props => props.theme.lLargeFontSize};
  text-align: ${props => props.align ? props.align  : "center"};
  color: ${props => props.light ? props.theme.lightFontColor : props.theme.darkFontColor};
  font-weight: bold;
  span{
  color: #F00;
  }
`;

export const H2 = H1.extend`
  font-size: ${props => props.theme.largeFontSize};
`;

export const H3 = H1.extend`
  font-size: ${props => props.theme.mediumFontSize};
`;

export const H5 = H1.extend`
  font-size: ${props => props.theme.littleFontSize};
`;


