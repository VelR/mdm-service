import React from 'react';
import Button from "@material-ui/core/Button/Button";
import TextField from "@material-ui/core/TextField/TextField";
import {Column, Row} from "./containers/Containers";
import styled from "styled-components"
import banner from '../Images/apply.png';


class MailForm extends React.Component {
    state = {
        name: '',
        phone: '',
        description: '',
        nameError: false,
        phoneError: false,
        descriptionError: false,
        isSend: false,
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
            [name + 'Error']: this.isEmpty(event.target.value)
        });
    };

    haveError = () => {
        let haveErrors = false;

        let isError = this.isEmpty(this.state.name);
        haveErrors += isError;
        this.setState({nameError: isError});

        isError = this.isEmpty(this.state.phone);
        haveErrors += isError;
        this.setState({phoneError: isError});

        isError = this.isEmpty(this.state.description);
        haveErrors += isError;
        this.setState({descriptionError: isError});

        return haveErrors;
    };

    sendMessage = () => {
        this.setState({isSend: true});
    };

    handleOnClick = () => {
        if (!this.haveError()) {
            this.sendMessage();
        }
    };

    isEmpty = str => str.toString().trim() === '';

    render() {
        const text = this.props.promotion ? <Text>Оставляя заявку вы получаете скидку 10%, подробности уточняйте в наших офисах или по телефону!</Text> : null;
        if (this.state.isSend) {
            return (
            <Row>
                <Image src={banner}/>
                <Text>
                    Спасибо за оставленную заявку, мы ответим вам после её рассмотрения!
                </Text>
            </Row>);
        }
        else {
            return (
                <form method="POST" action="send.php" id="sendForm" enableSubmissionForm="false">
                    <Column>
                        {text}
                        <TextField
                            inputProps={{maxLength: 50}}
                            label="Имя"
                            name="fio"
                            value={this.state.name}
                            onChange={this.handleChange('name')}
                            placeholder="Введите своё имя"
                            margin="normal"
                            required
                            error={this.state.nameError}
                        />
                        <TextField
                            inputProps={{maxLength: 12}}
                            label="Номер телефона"
                            name="email"
                            value={this.state.phone}
                            onChange={this.handleChange('phone')}
                            placeholder="Введите номер своего телефона, что бы мы могли ответить вам"
                            margin="normal"
                            required
                            error={this.state.phoneError}
                        />
                        <TextField
                            inputProps={{maxLength: 400}}
                            name="message"
                            label="Текст сообщения"
                            value={this.state.description}
                            onChange={this.handleChange('description')}
                            placeholder="Опишите проблему"
                            margin="normal"
                            multiline
                            required
                            error={this.state.descriptionError}
                        />
                        <StyledButton>
                            <Button variant="contained" type='submit' form="sendForm" color="primary" onClick={this.handleOnClick}>Отправить</Button>
                        </StyledButton>
                    </Column>
                </form>
            )
        }
    };
}

//#region Styled

const Image = styled.img`
    height: 150px;
    width: 150px;
`;

const Text = styled.div`
  margin: auto;
  color: black;
`;

const StyledButton = styled.div`
  margin: 1rem 0 0 auto;
`;


//#endregion

export default MailForm;