import React, {Component} from 'react';
import FirstPage from "./Pages/FirstPage";
import SecondPage from "./Pages/SecondPage";
import ThirdPage from "./Pages/ThirdPage";
import PromotionsPage from "./Pages/PromotionsPage";
import Contacts from "./Pages/Contacts";

class ContentPage extends Component {

    render() {
        return (
            <div>
                <FirstPage/>
                <PromotionsPage/>
                <SecondPage/>
                <ThirdPage/>
                <Contacts/>
            </div>
        )
    }
}

//#region Styled



//#endregion

export default ContentPage;