import React, {Component} from 'react';
import styled from 'styled-components'
import {Column, Page} from "./containers/Containers";
import logo from '../Images/logo.png';
import banner from '../Images/dark.jpg';
import {Link} from "./Components";

class Footer extends Component {
    render() {
        return (
            <Background>
                <Page>
                    <Grid>
                        <Cell>
                            <Logo src={logo} alt="MDM-logo"/>
                        </Cell>
                        <Cell>
                            <Column>
                                <Text>
                                    mdmservice.pro
                                </Text>
                                <Text>
                                    © Все права защищены
                                </Text>
                            </Column>
                        </Cell>
                        <Cell>
                            <Column>
                                <Text>Время работы</Text>
                                <Text>10:00 - 18:30</Text>
                            </Column>
                        </Cell>
                        <Cell>
                            <Column>
                                <Text>Телефон</Text>
                                <Text>
                                    <Link href="tel:+74842597878">
                                        +7 (4842) 59-78-78
                                    </Link>
                                </Text>
                            </Column>
                        </Cell>
                    </Grid>
                </Page>
            </Background>
        )
    }
}

//#region Styled

const Logo = styled.img`
  margin: auto 0;
  padding: 0 1em;
  height: 50px;
`;

const Text = styled.div`
  margin: 0 0 1em 0;
  text-align: center;
`;

const Cell = Column.extend`
    margin: auto;
    `;

const Grid = styled.div`
    display: grid;
    color: white;
    grid-template-columns: repeat(4, minmax(150px, 1fr));
    grid-gap: 1em;
    @media(max-width: 940px){
      grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
    }
`;

const Background = styled.footer`
    padding: 2em 0;
    margin: 0;
    width: 100%;
    background:  #262626 url(${banner});
    `;

//#endregion

export default Footer;