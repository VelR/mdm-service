import React, {Component} from 'react';
import {Row} from "../../containers/Containers";
import MaterialIcon from 'material-icons-react';
import {Link} from "../../Components";

class Phone extends Component {

    render() {
        return (
            <Wrapper>
                <MaterialIcon icon="phone" color="white"/>
                <Text>
                    {this.props.text}
                </Text>
            </Wrapper>
        )
    }
}

//#region Styled

const Wrapper = Row.extend`
  padding: 0.5em 0;
  
`;

const Text = Link.extend`
  margin: auto 1em;
`;

//#endregion

export default Phone;