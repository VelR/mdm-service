import React, {Component} from 'react';
import {Column} from "../../containers/Containers";
import styled from 'styled-components'
import {Button} from "../../Components";
import ModalSubmit from "../ModalSubmit";
import MailForm from "../../MailForm";

class OrderRepair extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isOpenCheck: false
        };
    };

    onCloseModal = () => {
        this.setState({isOpenSubmit: false});
    };
    submitOnClick = () => {
        this.setState({isOpenSubmit: true});
    };

    render() {
        return (
            <Wrapper>
                <Large>
                    СКИДКА 10%!
                </Large>
                <Text>
                    Оставьте заявку на сайте сейчас и получите скидку на ремонт
                </Text>
                <Wrapper>
                    <Button onClick={this.submitOnClick}>
                        Заказать ремонт
                    </Button>
                </Wrapper>
                <ModalSubmit header={"ЗАЯВКА НА ДИАГНОСТИКУ"} isOpen={this.state.isOpenSubmit} onClose={() => {this.setState({isOpenSubmit: false});}}>
                    <MailForm promotion/>
                </ModalSubmit>
            </Wrapper>
        )
    }
}

//#region Styled

const Wrapper = Column.extend`
  padding: 0.5em 0;
  margin: auto;
`;

const Text = styled.div`
  margin: auto 2em;
  text-align: center;
`;

const Large = Text.extend`
  font-size: 2em;  
`;

//#endregion

export default OrderRepair;