import React, {Component} from 'react';
import {Row} from "../../containers/Containers";
import styled from 'styled-components'
import MaterialIcon from 'material-icons-react';

class Address extends Component {

    render() {
        return (
            <Wrapper>
                <MaterialIcon icon="place" color="white"/>
                <Text>
                    {this.props.text}
                </Text>
            </Wrapper>
        )
    }
}

//#region Styled

const Wrapper = Row.extend`
  margin: auto;
  padding: 0.5em 0;
`;

const Text = styled.div`
  margin: auto 1em;  
`;

//#endregion

export default Address;