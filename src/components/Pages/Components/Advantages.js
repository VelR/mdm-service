import React, {Component} from 'react';
import styled from 'styled-components'
import {Column} from "../../containers/Containers";
import AdvantagesCard from "../../containers/AdvantagesCard";
import {H1} from "../../Components";


class Advantages extends Component {

    render() {
        return (
            <Wrap>
                <Title light align={"right"}>
                    Наши достоинства!
                </Title>
                <Grid>
                    <AdvantagesCard firstText={"Быстрая диагностика"}
                                    secondText={"в течении суток"}
                                    icon={"fast_forward"}/>
                    <AdvantagesCard firstText={"Высокое качество ремонта"}
                                    secondText={"любой техники"}
                                    icon={"high_quality"}/>
                    <AdvantagesCard firstText={"Гарантия на ремонт"}
                                    secondText={"1 месяц"}
                                    icon={"security"}/>
                    <AdvantagesCard firstText={"Постоянная поддержка"}
                                    secondText={"по телефону"}
                                    icon={"contact_phone"}/>
                    <AdvantagesCard firstText={"Индивидуальный подход"}
                                    secondText={"к каждой поломке"}
                                    icon={"person"}/>
                    <AdvantagesCard firstText={"Ремонт высокотехнологичных"}
                                    secondText={"устройств"}
                                    icon={"important_devices"}/>
                </Grid>
            </Wrap>
        )
    }
}

//#region Styled

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px,1fr));
  @media(max-width: 1200px){
    grid-template-columns: repeat(auto-fit, minmax(200px,1fr));
  }
  @media(max-width: 600px){
    grid-template-columns: repeat(auto-fit, minmax(250px,1fr));
  }
  grid-gap: 2vh;
`;

const Title = H1.extend`
  padding: 0.5em;
`;

const Wrap = Column.extend`
  width: 100%;
  justify-content: space-between;
  margin-left: auto;  
`;

//#endregion

export default Advantages;

