import React, {Component} from 'react';
import {Column} from "../../containers/Containers";
import styled from 'styled-components'
import {Link} from "../../Components";

class Contact extends Component {

    render() {
        const phone = "tel:"+this.props.phone;
        return (
            <Wrapper>
                <Text>
                    {this.props.address}
                </Text>
                <Text>
                    тел.
                    <Phone href={phone}>
                        {this.props.phone}
                    </Phone>
                </Text>
            </Wrapper>
        )
    }
}

//#region Styled

const Wrapper = Column.extend`
  padding: 0.5em 0;
`;

const Phone = Link.extend`
  margin: 0 0.5em ;
`;


const Text = styled.div`
  margin: auto 1em;
`;

//#endregion

export default Contact;