import React, {Component} from 'react';
import styled from 'styled-components'
import banner from '../../../Images/promotion.png';
import {Column} from "../../containers/Containers";

class Promotion extends Component {

    render() {
        return (
            <Wrapper>
                <Container>
                    <Star src={banner}/>
                    <Sale>
                        {this.props.sale}
                    </Sale>
                </Container>
                <Description>
                    {this.props.description}
                </Description>
            </Wrapper>
        )
    }
}

//#region Styled

const Container =  styled.div`
    position: relative;
    text-align: center;
`;

const Wrapper = Column.extend`
    align-items: center;
    flex-direction: column;
    justify-content: center;
`;

const Star = styled.img`
    height: 200px;
`;

const Sale = styled.div`
    position: absolute;
    font-size: ${props => props.theme.largeFontSize};
    font-weight: bold;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const Description = styled.div`
  text-align: center;
  font-size: ${props => props.theme.largeFontSize};
`;

//#endregion

export default Promotion;