import React, {Component} from 'react';
import {Column} from "../../containers/Containers";
import QuestionCard from "../../containers/QuestionCard";
import {H1} from "../../Components";


class Questions extends Component {

    render() {
        return (
            <Column>
                <Title light align={"left"}>
                    ЧАСТЫЕ ВОПРОСЫ!
                </Title>
                <QuestionCard question={"Сколько лет вы работаете?"}
                              response={"Мы работаем уже более 15-и лет!"}/>
                <QuestionCard question={"Есть ли гарантия на ремонт?"}
                              response={"Да. Мы даём гарантию на наш ремонт 30 дней."}/>
                <QuestionCard question={"Какой срок диагностики?"}
                              response={"В течении суток выявляем поломку."}/>
                <QuestionCard question={"Какой срок ремонта?"}
                              response={"От часа до 10 дней в зависимости от сложности."}/>
            </Column>
        )
    }
}

//#region Styled

const Title = H1.extend`
  padding: 0.5em;  
`;

//#endregion

export default Questions;

