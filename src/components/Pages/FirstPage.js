import React, {Component} from 'react';
import styled from 'styled-components'
import {Column, Page} from "../containers/Containers";
import banner from '../../Images/wallpaperDark.png';
import {Button, H1, H2} from "../Components";
import ModalSubmit from "./ModalSubmit";
import MailForm from "../MailForm";

class FirstPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isOpenCheck: false
        };
    };

    onCloseModal = () => {
        this.setState({isOpenSubmit: false});
        this.setState({isOpenCheck: false});
    };

    submitOnClick = () => {
        this.setState({isOpenSubmit: true});
    };

    checkOnClick = () => {
        this.setState({isOpenCheck: true});
    };



    render() {
        return (
            <Section id="main">
                <Background>
                    <Page>
                        <Wrapper>
                            <Column>
                                <Text>
                                    <H1>
                                        <span>MDM-SERVICE</span>
                                    </H1>
                                    <H2 light>
                                        <b>СЕТЬ СЕРВИСНЫХ ЦЕНТРОВ В КАЛУГЕ И КАЛУЖСКОЙ ОБЛАСТИ</b>
                                        <br/>
                                        ДОВЕРЬ ТЕХНИКУ ПРОФЕССИОНАЛАМ СВОЕГО ДЕЛА!
                                    </H2>
                                </Text>
                                <div>
                                    <Button onClick={this.submitOnClick}>
                                        ОСТАВИТЬ ЗАЯВКУ НА ДИАГНОСТИКУ
                                    </Button>
                                    <Button primary onClick={this.checkOnClick}>
                                        ПРОВЕРИТЬ СТАТУС ВАШЕГО РЕМОНТА
                                    </Button>
                                </div>
                            </Column>
                        </Wrapper>
                    </Page>
                </Background>
                <ModalSubmit header={"ЗАЯВКА НА ДИАГНОСТИКУ"} isOpen={this.state.isOpenSubmit} onClose={() => {this.setState({isOpenSubmit: false});}}>
                    <MailForm/>
                </ModalSubmit>
                <ModalSubmit header={"СТАТУС ВАШЕГО РЕМОНТА"} isOpen={this.state.isOpenCheck} onClose={()=> {this.setState({isOpenCheck: false});}}>
                    <Iframe src="https://serv4.pro/api/weblk_kaluga/index.php"/>
                </ModalSubmit>
            </Section>
        )
    }
}

//#region Styled

const Iframe = styled.iframe`
  width: 850px;
  height: 600px;
  @media(max-width: 850px){
  width: 100%;
  height: 350px;  
  }
`;

const Section = styled.section`
  box-sizing: border-box;
  display: block;  
  height: 100vh;
`;

const Background = styled.div`
  width: 100%;
  height: 100vh;
  background: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(${banner}) repeat; /* Добавляем фон */
  background-size: 100% auto;
  @media(max-width: 1500px)
  {
    background-size: auto 100%;
  }
`;

const Text = Column.extend`
  margin: 0 0 1em 0;
`;

const Wrapper = styled.div`
  margin: auto;
  width: 90%;
  color: white;
  align-content: center;
  text-align: center;
  @media(max-width: 600px){
  }
`;

//#endregion

export default FirstPage;