import React, {Component} from 'react';
import styled from 'styled-components'
import {Page} from "../containers/Containers";
import img1 from '../../Images/giro_small.png';
import img2 from '../../Images/notebooke_small.png';
import img3 from '../../Images/monokoles.jpg';
import img4 from '../../Images/phone_small.png';
import img5 from '../../Images/kvadro_small.png';
import img6 from '../../Images/camera_small.png';
import img7 from '../../Images/robot.png';
import img8 from '../../Images/watch_small.png';
import img9 from '../../Images/electroWheels_small.png';
import img10 from '../../Images/segway_small.png';
import Card from "../containers/Card";
import {H1} from "../Components";

class SecondPage extends Component {

    render() {
        return (
            <Background id="service">
                <Page>
                    <H1>
                        <span>MDM-SERVICE</span> ЭТО КАЧЕСТВЕННЫЙ РЕМОНТ
                    </H1>
                    <Grid>
                        <Card image={img1} text={"ГИРОСКУТЕРОВ"}/>
                        <Card image={img2} text={"КОМПЬЮТЕРОВ И НОУТБУКОВ"}/>
                        <Card image={img3} text={"МОНОКОЛЕС"}/>
                        <Card image={img4} text={"ТЕЛЕФОНОВ И ПЛАНШЕТОВ"}/>
                        <Card image={img5} text={"КВАДРОКОПТЕРОВ"}/>
                        <Card image={img6} text={"ФОТОАППАРАТОВ И ВИДЕОКАМЕР"}/>
                        <Card image={img7} text={"РОБОТОВ - ПЫЛЕСОСОВ"}/>
                        <Card image={img8} text={"СМАРТ ЧАСОВ"}/>
                        <Card image={img9} text={"ЭЛЕКТРО ВЕЛОСИПЕДОВ"}/>
                        <Card image={img10} text={"SEGWAY И АНАЛОГОВ"}/>
                    </Grid>
                </Page>
            </Background>
        )
    }
}

//#region Styled

 const Grid = styled.div`
  display: grid;
  font-size: ${props => props.theme.largeFontSize};
  grid-template-columns: repeat(auto-fill, minmax(300px,1fr));
  grid-auto-rows: 180px;
  grid-gap: 1em;  
  @media (max-width: 800px) {
     grid-template-columns: repeat(auto-fill, minmax(250px,1fr));
     grid-auto-rows: 120px;
  }
  
`;

const Background = styled.div`
  padding: 2rem 0;
  margin: 0;
  width: 100%;
  background: white; /* Добавляем фон */
`;

//#endregion

export default SecondPage;