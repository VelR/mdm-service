import React, {Component} from 'react';
import styled from 'styled-components'
import {Column, Row} from "../containers/Containers";
import {H3} from "../Components";
import MaterialIcon from 'material-icons-react';


class ModalSubmit extends Component {
    onCloseClick = () => {
        this.props.onClose();
    };

    render() {
        if(this.props.isOpen)
        {
            return (
                <div>
                    <OverLay onClick={this.onCloseClick}/>
                    <ModalWrapper>
                        <Header>
                            <Title light>
                                {this.props.header}
                            </Title>
                            <CloseIcon onClick={this.onCloseClick}>
                                <MaterialIcon icon="close" color='white'/>
                            </CloseIcon>
                        </Header>
                        <Content>
                            {this.props.children}
                        </Content>
                    </ModalWrapper>
                </div>
            )
        }
        else {
            return(
                null
            )
        }
    }
}

//#region Styled

const Title = H3.extend`
 padding: 0.5rem 0;
`;

const CloseIcon = styled.div`
  cursor: pointer;
  margin: auto 0;
`;

const OverLay = styled.div`
  position: fixed;
  z-index: 101;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background: rgba(0,0,0,0.4);  
`;

const ModalWrapper = Column.extend`
  position: fixed;
  z-index: 102;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  min-width: 600px;
  background: white;  
  box-shadow: 0 0 1rem rgba(0,0,0,0.8);
  @media(max-width: 850px){
  width: 90%;
  max-height: 90%;
  min-width: 0;
  }
`;

const Header = Row.extend`
  justify-content: space-between;
  padding: 1rem;  
  color: #fff;
  background: ${props => props.theme.linkColor};
`;

const Content = styled.section`
  padding: 1rem;
`;

//#endregion

export default ModalSubmit;