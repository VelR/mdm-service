import React, {Component} from 'react';
import styled from 'styled-components'
import {Column, Page} from "../containers/Containers";
import Promotion from "./Components/Promotion";
import {H1} from "../Components";

class PromotionsPage extends Component {

    render() {
        return (
            <Background id="promotion">
                <Page>
                    <Title>
                        НАШИ АКЦИИ!
                    </Title>
                    <Grid>
                        <Cell>
                            <Promotion sale='5%' description='Всем пенсионерам!'/>
                        </Cell>
                        <Cell>
                            <Promotion sale='10%' description='Каждое воскресенье!'/>
                        </Cell>
                        <Cell>
                            <Promotion sale='3-10%' description='Система накопительных скидок!'/>
                        </Cell>
                    </Grid>
                </Page>
            </Background>
        )
    }
}

//#region Styled

const Cell = Column.extend`
    height: 100%;
`;

const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
    grid-gap: 1em;
    @media(max-width: 900px){
      grid-template-columns: 1fr;
    }
`;

const Title = H1.extend`
  padding: 0 0 1em 0;
`;

const Background = styled.div`
  padding: 2em 0;
  margin: 0;
  width: 100%;
`;


//#endregion

export default PromotionsPage;