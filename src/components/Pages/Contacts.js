import React, {Component} from 'react';
import styled from 'styled-components'
import {Page, } from "../containers/Containers";
import banner from '../../Images/wallpaperDark.png';
import Contact from "./Components/Contact";
import {H2} from "../Components";
import OrderRepair from "./Components/OrderRepair";

class Contacts extends Component {

    render() {
        return (
            <Background id="contacts">
                <Wrapper>
                    <Grid>
                        <Cell>
                            <Title light>
                                НАШИ КОНТАКТЫ
                            </Title>
                            <Contact address="г. Калуга ул. Кирова 78" phone="8-4842-59-78-78"/>
                            <Contact address="г. Калуга ул. Кирова 7/47" phone="8-920-615-11-17"/>
                            <Contact address="г. Обнинск ул. Аксенова 18" phone="8-920-880-88-00"/>
                        </Cell>
                        <OrderRepair/>
                    </Grid>
                </Wrapper>
            </Background>
        )
    }
}

//#region Styled

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(240px,1fr));
  grid-gap: 2em;
`;

const Title = H2.extend`
  text-align: left;
  @media(max-width: 600px){
    text-align: center;
  }
`;

const Cell = styled.div`
  margin: auto;
`;

const Background = styled.section`
  background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.3)), url(${banner}) repeat; /* Добавляем фон */
  color: #FFF;
  padding: 3em 0;
`;


const Wrapper = Page.extend`
  @media(max-width: 600px){
  text-align: center;
  }
`;

//#endregion

export default Contacts;