import React, {Component} from 'react';
import styled from 'styled-components'
import {Page} from "../containers/Containers";
import banner from '../../Images/banner2.jpg'
import Questions from "./Components/Questions";
import Advantages from "./Components/Advantages";

class ThirdPage extends Component {

    render() {
        return (
            <Section id="about">
                <Background>
                    <Page>
                        <Grid>
                            <Questions/>
                            <Advantages/>
                        </Grid>
                    </Page>
                </Background>
            </Section>
        )
    }
}

//#region Styled

const Section = styled.section`
  box-sizing: border-box;
  display: block;
  color: white;
`;

const Background = styled.div`
  padding: 2em 0;
  width: 100%;
  background: linear-gradient(rgba(0, 0, 0, 0.8),rgba(0, 0, 0, 0.8)), url(${banner}) no-repeat; /* Добавляем фон */
  background-size: 100% auto;
  @media(max-width: 1200px){
   height: 100%;
   background-size: auto 100%;
   background-position: 50%;
  }
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  @media(max-width: 1200px)
  {
    grid-template-columns: 1fr;
  }
  grid-gap: 2em;
`;
//#endregion

export default ThirdPage;