import React, {Component} from 'react';
import {ThemeProvider} from 'styled-components';
import Header from "./components/Header";
import './App.css';
import ContentPage from "./components/ContentPage";
import Footer from "./components/Footer";

const theme = {
    lightFontColor: `#FFF`,
    darkFontColor: `#000`,
    linkColor: '#55ACEE',
    linkHoverColor: '#1689e0',
    primaryColor: '#5cb85c',
    primaryHoverColor: '#4cae4c',

    littleFontSize: '12px',
    defaultFontSize: '16px',
    mediumFontSize: '16px',
    largeFontSize: '24px',
    lLargeFontSize: '32px',
};

class App extends Component {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <div>
                    <Header/>
                    <ContentPage/>
                    <Footer/>
                </div>
            </ThemeProvider>
        );
    }
}

export default App;
